--Refer readme.md to download the dependency files
Add jar hivemall-all-0.6.0-incubating.jar;
Source define-all.hive;

--Code referred fromhttps://hivemall.incubator.apache.org/userguide/ft_engineering/tfidf.html

--Define macros
create temporary macro max2(x INT, y INT)
if(x>y,x,y);

-- create temporary macro idf(df_t INT, n_docs INT)
-- (log(10, CAST(n_docs as FLOAT)/max2(1,df_t)) + 1.0);
create temporary macro tfidf(tf FLOAT, df_t INT, n_docs INT)
tf * (log(10, CAST(n_docs as FLOAT)/max2(1,df_t)) + 1.0);

--Data preparation
create external table wikipage (
  docid int,
  page string
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
STORED AS TEXTFILE;

LOAD DATA LOCAL INPATH '/top10body.tsv' INTO TABLE wikipage;

create or replace view wikipage_exploded
as
select
  docid,
  word
from
  wikipage LATERAL VIEW explode(tokenize(page,true)) t as word
where
  not is_stopword(word);

--Define views for tf-idf
create or replace view term_frequency
as
select
  docid,
  word,
  freq
from (
select
  docid,
  tf(word) as word2freq
from
  wikipage_exploded
group by
  docid
) t
LATERAL VIEW explode(word2freq) t2 as word, freq;

create or replace view document_frequency
as
select
  word,
  count(distinct docid) docs
from
  wikipage_exploded
group by
  word;

--TF-IDF calculation for each word and ownerid pair
-- set the total number of documents
select count(distinct docid) from wikipage;
set hivevar:n_docs=3;

create or replace view tfidf
as
select
  tf.docid,
  tf.word,
  -- tf.freq * (log(10, CAST(${n_docs} as FLOAT)/max2(1,df.docs)) + 1.0) as tfidf
  tfidf(tf.freq, df.docs, ${n_docs}) as tfidf
from
  term_frequency tf
  JOIN document_frequency df ON (tf.word = df.word)
order by
  tfidf desc;
