--This file includes all the Pig Latin scripts used for the Assignment.

--Register Piggybank.jar if the uploaded csv file fields have commas and newlines.
register 'hdfs:///pig/piggybank.jar';

--Load all the csv files using CSVExcelStorage
w = LOAD '/pig/QueryResults1.csv' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE') AS (postid:int,typeid:int,ansid:int,pid:int,cd:datetime,dd:datetime,score:int,viewcount:int,body:chararray,ownerid:int,ownername:chararray,lasteditorid:int,lasteditorname:chararray,lasteditdate:datetime,lastactdate:datetime,title:chararray,tags:chararray,anscount:int,commentcount:int,favcount:int,closeddate:datetime,owneddate:datetime,license:chararray);
x = LOAD '/pig/QueryResults2.csv' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE') AS (postid:int,typeid:int,ansid:int,pid:int,cd:datetime,dd:datetime,score:int,viewcount:int,body:chararray,ownerid:int,ownername:chararray,lasteditorid:int,lasteditorname:chararray,lasteditdate:datetime,lastactdate:datetime,title:chararray,tags:chararray,anscount:int,commentcount:int,favcount:int,closeddate:datetime,owneddate:datetime,license:chararray);
y = LOAD '/pig/QueryResults3.csv' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE') AS (postid:int,typeid:int,ansid:int,pid:int,cd:datetime,dd:datetime,score:int,viewcount:int,body:chararray,ownerid:int,ownername:chararray,lasteditorid:int,lasteditorname:chararray,lasteditdate:datetime,lastactdate:datetime,title:chararray,tags:chararray,anscount:int,commentcount:int,favcount:int,closeddate:datetime,owneddate:datetime,license:chararray);
z = LOAD '/pig/QueryResults4.csv' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE') AS (postid:int,typeid:int,ansid:int,pid:int,cd:datetime,dd:datetime,score:int,viewcount:int,body:chararray,ownerid:int,ownername:chararray,lasteditorid:int,lasteditorname:chararray,lasteditdate:datetime,lastactdate:datetime,title:chararray,tags:chararray,anscount:int,commentcount:int,favcount:int,closeddate:datetime,owneddate:datetime,license:chararray);

--Remove the first row from w,x,y,z because it is the label for each column.
--Using the Filter command I am filtering out the header using Body label. Any other label can also be used.
dw = FILTER w BY $8!='Body';
dx = FILTER x BY $8!='Body';
dy = FILTER y BY $8!='Body';
dz = FILTER z BY $8!='Body';

--The body field has unwanted breaks,newlines,commas and tags which will be cleaned here.
--The GENERATE function creates tables with only specified columns.
nocommaw = foreach w generate postid,score,viewcount,ownerid,title,tags,REPLACE(REPLACE(REPLACE(REPLACE(body,'\\n',''),'\\r',''),'\\r\\n',''),'<*>','') AS body;
nocommax = foreach x generate postid,score,viewcount,ownerid,title,tags,REPLACE(REPLACE(REPLACE(REPLACE(body,'\\n',''),'\\r',''),'\\r\\n',''),'<*>','') AS body;
nocommay = foreach y generate postid,score,viewcount,ownerid,title,tags,REPLACE(REPLACE(REPLACE(REPLACE(body,'\\n',''),'\\r',''),'\\r\\n',''),'<*>','') AS body;
nocommaz = foreach z generate postid,score,viewcount,ownerid,title,tags,REPLACE(REPLACE(REPLACE(REPLACE(body,'\\n',''),'\\r',''),'\\r\\n',''),'<*>','') AS body;

--Combine the results of all the above tables into one using UNION command.
onetable = UNION ONSCHEMA nocommaz,nocommay,nocommax,nocommaw;

--Store the results into hdfs.
STORE onetable INTO 'hdfs:///pig/output_pig/' USING PigStorage;
