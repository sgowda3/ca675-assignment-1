# CA675 - assignment 1

Cloud Assignment on map-reduce, pig and hive.

-> The pig_operations.pig file includes all the pig latin scripts used in the Assignment

-> The hive_operations.sql file includes all the hive commands and queries. The results of the queries are stored in results_query1.csv and results_query2.csv

-> Download the dependencies from https://hivemall.incubator.apache.org/userguide/getting_started/installation.html to calculate the TF-IDF

Add the files to HDFS and go to tfidf.sql file to calculate the TF-IDF

The results from tfidf.sql is present in results_tfidf.txt

All the screenshots and documentation is present in the attached documentation.pdf
