--This file includes all the hive sql commands used for the Assignment

--Create database
create database posts;

--Use cloud database
use posts;

--Create table
create table data (
postid int,
score int,
viewcount int,
ownerid int,
title string,
tags string,
body string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE ;

--Load the contents into the table database
load data inpath 'hdfs:///pig/output_pig/part-m-00000.csv' into table data;

--OpenCSVSerde changes the type of all the fields to string, create another table to redefine types
set hive.support.quoted.identifiers=none;
create table if not exists demo as
select `(postid|score|viewcount|ownerid)?+.+`
    , cast(postid as int) as postid
    , cast(score as int) as score
    , cast(viewcount as int) as viewcount
    , cast(ownerid as int) as ownerid
    from data;

--Perform the Queries on demo tables

--Query 1 - get the top 10 posts based on the field score
Select postid,score,title from demo order by score desc limit 10;

--Query 2 - get top 10 users based on post score
Select postid,score,ownerid from demo order by score desc limit 10;
--Store the results to use it to calculate tfidf
hive -e 'Select * from demo order by score desc limit 10' > top10body.tsv

--Query 3 - Number of distinct users who used Hadoop in any of their posts
Select count(distinct ownerid) from demo where body like '%Hadoop%' or title like '%Hadoop%' or tags like '%Hadoop%';
